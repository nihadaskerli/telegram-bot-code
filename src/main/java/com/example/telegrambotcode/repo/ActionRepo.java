package com.example.telegrambotcode.repo;

import com.example.telegrambotcode.models.Action;


import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepo extends JpaRepository<Action, Long> {
    Action findByButtonName(String buttonKey);


}

