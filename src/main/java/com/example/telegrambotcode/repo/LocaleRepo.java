package com.example.telegrambotcode.repo;


import com.example.telegrambotcode.models.Locale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocaleRepo extends JpaRepository<Locale, Long> {
    Locale findByKey(String key);
    List<Locale>getByKey(String key);
    List<Locale>findByKeyAndLang(String key,String lang);
}
