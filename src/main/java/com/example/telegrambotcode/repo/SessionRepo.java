package com.example.telegrambotcode.repo;

import com.example.telegrambotcode.models.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepo extends JpaRepository<Session,Long> {
    Session findByChatId(Long chatId);


}
