package com.example.telegrambotcode.repo;

import com.example.telegrambotcode.models.Locale;
import com.example.telegrambotcode.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepo extends JpaRepository<Question,Long> {
    Question findByQuestionKey(String key);
    Question getById(Long id);
}
