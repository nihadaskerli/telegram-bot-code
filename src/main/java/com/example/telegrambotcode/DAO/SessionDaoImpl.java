package com.example.telegrambotcode.DAO;

import com.example.telegrambotcode.models.forRedis.HelperRedis;
import jakarta.annotation.Resource;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;

import java.util.Map;

@RequiredArgsConstructor
@Repository
public class SessionDaoImpl implements SessionDao {

    private final String hashReference = "HelperRedis";

    @Resource(name = "redisTemplate")
    private HashOperations<String, Long, HelperRedis> hashOperations;


    @Override
    public HelperRedis saveSession(HelperRedis sessionRedis) {
        hashOperations.putIfAbsent(hashReference, sessionRedis.getChatId(), sessionRedis);
        return sessionRedis;
    }

    @Override
    public Map<Long, HelperRedis> getAll() {
        return hashOperations.entries(hashReference);
    }

    @Override
    public HelperRedis getChatId(Long id) {
        return hashOperations.get(hashReference, id);
    }

    @Override
    public void updateSession(HelperRedis redis) {
        hashOperations.put(hashReference, redis.getChatId(), redis);
    }

//    @Override
//    public void deleteSession(Integer id) {
//        hashOperations.delete(hashReference, id);
//    }
//
//    @Override
//    public void saveAllSession(Map<Integer, HelperRedis> map) {
//
//        hashOperations.putAll(hashReference, map);
//    }


}
