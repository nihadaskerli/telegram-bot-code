package com.example.telegrambotcode.DAO;

import com.example.telegrambotcode.models.forRedis.HelperRedis;

import java.util.Map;

public interface SessionDao {

    HelperRedis saveSession(HelperRedis sessionRedis);

    Map<Long, HelperRedis> getAll();
    HelperRedis getChatId(Long id);
    void updateSession(HelperRedis redis);



//    void deleteSession(Integer id);
//    void saveAllSession(Map<Integer, SessionRedis> map);
}
