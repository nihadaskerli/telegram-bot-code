package com.example.telegrambotcode.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "sessions")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private Long chatId;
    @OneToMany(mappedBy = "session",cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Answer> answer;

    public void setAnswer(List<Answer> answers) {
        for (Answer answer1 : answers) {
            answer1.setSession(this);
        }
        this.answer = answers;


    }

}
