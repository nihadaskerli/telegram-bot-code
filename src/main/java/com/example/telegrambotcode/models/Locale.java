package com.example.telegrambotcode.models;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="locale")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Locale {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String key;
    private String value;
    private String lang;


}
