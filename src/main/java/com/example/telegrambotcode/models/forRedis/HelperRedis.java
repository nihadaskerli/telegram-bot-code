package com.example.telegrambotcode.models.forRedis;

import jakarta.persistence.Id;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@RedisHash(value = "HelperSession")
public class HelperRedis implements Serializable {

//    @Id
//    private Integer chatId;
//    private String clientId;
//    @Column(columnDefinition = "jsonb")
//    private String answer;

    @Id
    private Long chatId;
    private String nextQuestion;
    boolean isActive;
    private String lang;
}
