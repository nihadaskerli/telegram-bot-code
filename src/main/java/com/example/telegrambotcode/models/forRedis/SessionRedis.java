package com.example.telegrambotcode.models.forRedis;


import com.example.telegrambotcode.models.Answer;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RedisHash(value = "redisSession")
public class SessionRedis {
    @Id
    private Integer sessionId;
    private Integer chatId;

    List<Answer> answerList;
}
