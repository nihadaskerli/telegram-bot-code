package com.example.telegrambotcode.models;

import com.example.telegrambotcode.repo.SessionRepo;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="answer")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String dil;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "session_id",referencedColumnName = "id")
    private Session session;
}
