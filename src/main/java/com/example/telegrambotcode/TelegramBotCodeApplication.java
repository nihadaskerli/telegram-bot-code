package com.example.telegrambotcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelegramBotCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramBotCodeApplication.class, args);
    }

}
