package com.example.telegrambotcode.sevice;

import com.example.telegrambotcode.models.Question;
import com.example.telegrambotcode.repo.QuestionRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QuestionService {
    ActionService actionService;
    private final QuestionRepo questionRepo;

    public String getByQuestionKey(String key){
        return questionRepo.findByQuestionKey(key).getActionList().get(0).getNextQuestion();
    }


}
