package com.example.telegrambotcode.sevice;

import com.example.telegrambotcode.models.Locale;
import com.example.telegrambotcode.repo.LocaleRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LocaleService {
    private final LocaleRepo localeRepo;

    public String getStartQuestion(String key) {
        return localeRepo.findByKey(key).getValue();
    }

    public List<Locale> getStartLanguage(String key) {
        return localeRepo.getByKey(key);
    }

    public List<Locale> getLocaleKeyandLand(String key, String language) {
//        System.out.println(localeRepo.findByKeyAndLang("category","English").get(0).getValue());
        return localeRepo.findByKeyAndLang(key, language);
    }
}
