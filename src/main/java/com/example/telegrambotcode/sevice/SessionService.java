package com.example.telegrambotcode.sevice;

import com.example.telegrambotcode.models.Session;
import com.example.telegrambotcode.repo.SessionRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SessionService {
    public final SessionRepo sesionRepo;
    public void saveSesion(Session sesion){
        sesionRepo.save(sesion);
    }
    public Session getByChatId(Long chatId){
        return sesionRepo.findByChatId(chatId);
    }

}
