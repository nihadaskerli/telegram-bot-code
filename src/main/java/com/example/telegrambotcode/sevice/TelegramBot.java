package com.example.telegrambotcode.sevice;

import com.example.telegrambotcode.config.BotConfig;
import com.example.telegrambotcode.controller.RedisController;
import com.example.telegrambotcode.models.Answer;
import com.example.telegrambotcode.models.Session;
import com.example.telegrambotcode.models.forRedis.HelperRedis;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TelegramBot extends TelegramLongPollingBot {


    private final BotConfig botConfig;
    @Autowired
    QuestionService questionService;
    @Autowired
    LocaleService localeService;
    @Autowired
    SessionService sesionService;
    @Autowired
    ActionService actionService;

    @Override
    public String getBotUsername() {
        return botConfig.getName();
    }

    @Override
    public String getBotToken() {
        return botConfig.getToken();
    }

//    public static HashMap<String,String > language = new ArrayList();


    @Override
    public void onUpdateReceived(Update update) {
        SendMessage response = new SendMessage();
        response.setChatId(update.getMessage().getChatId());
//        redisController.init(update);

//        redisController.createSession(new HelperRedis(update.getMessage().getChatId(),
//                "category", true, "English"));


//        start
//        if (update.getMessage().getText().equals("/start")) {
//            response.setText(localeService.getLocaleKeyandLand(update.getMessage().getText(), "Azərbaycan").get(0).getValue());
//        }
//        response.setReplyMarkup(setButton(update));
//        List<Answer> list = new ArrayList<>();
//        Session sesion = new Session(null, update.getMessage().getChatId(), update.getMessage().getText(), list);
//        list.add(new Answer(null, update.getMessage().getText(), sesion));
//        if(update.getMessage().getText().equals("Azərbaycan")||update.getMessage().getText().equals("English")||update.getMessage().getText().equals("Русский")){
//            sesionService.saveSesion(sesion);
//            response.setText(localeService.getLocaleKeyandLand(actionService.getNextQuestion("lang").getNextQuestion(),
//                    sesionService.getByChatId(update.getMessage().getChatId()).getLanguage()).get(0).getValue());
////            response.setReplyMarkup(setButton2(update));
//        }
//
//        try {
//            execute(response);
//
//        } catch (TelegramApiException e) {
//            throw new RuntimeException(e);
//        }
    }


    @SneakyThrows
    public ReplyKeyboard setButton(Update update) {
        SendMessage message = new SendMessage();
        message.setChatId(update.getMessage().getChatId());

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboardList = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        if (update.getMessage().getText().equals("/start")) {
            localeService.getStartLanguage("lang").stream().forEach(elem -> {
                KeyboardButton button1 = new KeyboardButton();
                button1.setText(elem.getValue());
                row.add(button1);
            });
        } else {
            localeService.getStartLanguage("ctgry").stream().forEach(elem -> {
                KeyboardButton button1 = new KeyboardButton();
                button1.setText(elem.getValue());
                row.add(button1);
            });
        }
        keyboardList.add(row);
        keyboardMarkup.setKeyboard(keyboardList);
        message.setReplyMarkup(keyboardMarkup);
        return message.getReplyMarkup();
    }
}


