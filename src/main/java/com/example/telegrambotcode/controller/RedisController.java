package com.example.telegrambotcode.controller;

import com.example.telegrambotcode.DAO.SessionDao;
import com.example.telegrambotcode.models.forRedis.HelperRedis;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Map;

//@RestController
@RequiredArgsConstructor
//@RequestMapping("/redis")
public class RedisController {

    private final SessionDao sessionDao;


    @PostConstruct
    public void init() {

        HelperRedis helperRedis = HelperRedis.
                builder()
                .chatId(1l)
                .nextQuestion("category")
                .isActive(true)
                .lang("Azerbaycan").build();

        sessionDao.saveSession(helperRedis);

    }


//    public void post() {
//        sessionDao.saveSession(new HelperRedis(1l,"category",true,"Azerbaycan"));
//    }
//
//    public void getAll() {
//        System.out.println(sessionDao.getAll());
//    }
//
//
//    @PostMapping
//    public ResponseEntity<HelperRedis> createSession(@RequestBody HelperRedis sessionRedis) {
//        return ResponseEntity.ok(sessionDao.saveSession(sessionRedis));
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<HelperRedis> getById(@PathVariable long id) {
//        return ResponseEntity.ok(sessionDao.getChatId(id));
//    }
//
//    @GetMapping("/all")
//    public ResponseEntity<Map<Long, HelperRedis>> getAll() {
//        return ResponseEntity.ok(sessionDao.getAll());
//    }
}
